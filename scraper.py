"""
Author: Alain Moré Maceda
Scraper de productos Juguetron MX
"""
import csv
import json
import logging
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_opt.add_experimental_option("prefs",prefs)
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 10)
actions = ActionChains(driver)

existing_products = []


def get_existing_products(connection):
    params = dict(COUNTRY='MX')

    select = """
        SELECT SKU FROM {{COUNTRY}}_WRITABLE.JUGUETRON_PRODUCTS;
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(select)
    formatted_template = template.render(**params)
    log.info(formatted_template)
    try:
        df_existing_products = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info(df_existing_products)
        for element in df_existing_products.values.tolist():
            existing_products.append(element[0])
    except Exception as ex:
        log.error(ex)


def get_dom(url):
    """
    gets the DOM of the requested url using selenium
    """
    try:
        driver.get(url)
    except:
        try:
            time.sleep(5)
            driver.switchTo().alert().accept();
            time.sleep(5)
            driver.get(url)
        except:
            log.info("Error con alert")


def do_smooth_scroll():
    """
    Smoothly scrolls the current page all the way to the bottom,
    giving time to results to load.
    """
    time.sleep(3)
    total_height = int(driver.execute_script("return document.body.scrollHeight"))

    for i in range(1, total_height, 5):
        driver.execute_script("window.scrollTo(0, {});".format(i))

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight - 2100);")


def insert_product(connection, product):
    """
    Inserts current processed product into Snowflake
    """
    insert = """
        INSERT INTO MX_WRITABLE.JUGUETRON_PRODUCTS 
            (CATEGORY,URL,NAME,PRICE_1,PICTURES,SUMMARY,SKU,UPC,SCRAPE_DATETIME)
        VALUES(
            '{{prod_category}}',
            '{{prod_url}}',
            '{{prod_name}}',
            {{price}},
            '{{pictures}}',
            '{{prod_desc}}',
            '{{sku}}',
            '{{upc}}',
            '{{scrape_datetime}}'  
        )     
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**product)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Insert OK")
        log.info(df_insert)
    except Exception as ex:
        log.info("Insert ERROR")
        log.error(ex)


def get_prod_details_from_listing(prod_category, result):
    """
    Obtiene los datos de los productos desplegados desde el
    listing de categoría. Considera todas las posibles variantes
    de un producto como un producto diferente.
    """
    prod_url = ""
    prod_url_tag = result.find("a")
    if prod_url_tag:
        prod_url = prod_url_tag["href"]
    else:
        log.info("No se pudo obtener prod_url o prod_name")

    product_info = {"prod_category": prod_category, "prod_url": prod_url}

    return product_info


def start_scraping(url, connection):
    """
    Scraping main function, orchestrates full process
    """

    # En este bloque se obtienen los productos de la categoria.

    get_dom(url)
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(3)
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")
    prod_category = cat_soup.find(
        "span", {"data-ui-id": "page-title-wrapper"}
    ).text.strip()
    #try:
    #    view_more_element = driver.find_element_by_xpath("//p[text()=' Ver más ']")
    #    view_more_element.click()
    #    log.info("Init: Boton VER MAS presionado")
    #except:
    #    view_more_element = None
    #    log.info("Init: No hay boton VER MAS")

    #i = 0
    #while view_more_element:
    #    i = i + 1
    #    log.info("")
    #    log.info("Ciclo %i", i)
    #    time.sleep(3.5)
    #    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    #    time.sleep(0.5)
    #    log.info("scroll down done")
    #    try:
    #        view_more_element = driver.find_element_by_xpath("//p[text()=' Ver más ']")
    #        view_more_element.click()
    #        log.info("Boton VER MAS %i presionado", i)
    #    except:
    #        view_more_element = None

    try:
        view_more_element = driver.find_element_by_xpath("//div[@class='ias-trigger ias-trigger-next']")
    except:
        log.info("No more articles!!")
        view_more_element = None

    i = 0
    while view_more_element:
        i = i + 1
        log.info("")
        log.info("Ciclo %i", i)
        time.sleep(3.5)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(0.5)
        log.info("scroll down done")
        try:
            view_more_element = driver.find_element_by_xpath("//div[@class='ias-trigger ias-trigger-next']")
            view_more_element.click()
            log.info("Boton VER MAS %i presionado", i)
        except:
            no_more_element = None
            attempts = 0
            while no_more_element == None:
                if attempts >= 50:
                    log.info("Se hicieron mas de 50 intentos saliendo while 1")
                    break
                try:
                    no_more_element = driver.find_element_by_xpath("//div[@class='ias-noneleft']")
                    view_more_element = None
                    log.info("Confirmado, no hay mas articulos")
                except:
                    log.info("Hubo un problema al obtener ias-noneleft, esperando para probar otra vez")
                    time.sleep(10)
                    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                    try:
                        view_more_element = driver.find_element_by_xpath("//div[@class='ias-trigger ias-trigger-next']")
                        view_more_element.click()
                        log.info("Se pudo hacer click en VER MAS en un segundo intento")
                        break
                    except:
                        no_more_element = None
                        view_more_element = None
                        log.info("No se pudo hacer click en un segundo intento, probando en 10 segundos otra vez...")
                        attempts = attempts + 1
            if attempts >= 20:
                log.info("Se hicieron mas de 50 intentos saliendo while 2")
                break            

    log.info("No more products!")
    time.sleep(1)
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")

    results = cat_soup.find_all("div", {"class": "product-item-info"})
    log.info("")
    log.info("Total productos: %i", len(results))
    log.info("")
    # Obtener los detalles de cada producto, desde la pagina de resultados
    products = []
    added_products = []
    i = 0
    for result in results:
        product_info = []
        log.info(">>>>>>>>>>>>>>>>>>>>")
        log.info("Producto: %i", i)
        product_info = get_prod_details_from_listing(prod_category, result)

        if product_info["prod_url"] not in added_products:
            products.append(product_info)
            log.info("Nuevo producto agregado: ")
            log.info(product_info)
            added_products.append(product_info["prod_url"])
        else:
            log.info("Producto duplicado: ")
            log.info(product_info)            

        i = i + 1

    print(products)
    print("-----")
    print(added_products)
    print("")
    print("")
    print("")

    # Obtener detalles desde la página del producto
    for product in products:
        get_dom(product["prod_url"])
        time.sleep(1)
        prod_soup = BeautifulSoup(driver.page_source, "html.parser")

        # SKU
        sku = ""
        sku_parent_tag = prod_soup.find("div", {"class": "product attribute sku"})
        if sku_parent_tag:
            sku_tag = sku_parent_tag.find("div", {"class": "value"})
            if sku_tag:
                sku = sku_tag.text.strip().replace("'", "")
                product["sku"] = sku
            else:
                log.info("No se pudo obtener sku")
        else:
            log.info("No se pudo obtener sku parent")

        # NAME
        prod_name = ""
        prod_name_tag = prod_soup.find("span", {"data-ui-id": "page-title-wrapper"})
        if prod_name_tag:
            prod_name = prod_name_tag.text.strip().replace("'", "")
            product["prod_name"] = prod_name
        else:
            log.info("No se pudo obtener prod_name")

        # DESCRIPTION
        prod_desc = ""
        prod_desc_parent_tag = prod_soup.find(
            "div", {"class": "product attribute description"}
        )
        if prod_desc_parent_tag:
            prod_desc_tag = prod_desc_parent_tag.find("div", {"class": "value"})
            if prod_desc_tag:
                prod_desc = (
                    prod_desc_tag.text.strip()
                    .replace("'", "")
                    .replace("\n", " ")
                    .replace("\r", "")
                )
                product["prod_desc"] = prod_desc
            else:
                log.info("No se pudo obtener prod_desc 1")
        else:
            prod_desc_parent_tag = prod_soup.find(
                "div", {"class": "product attribute overview"}
            )
            if prod_desc_parent_tag:
                prod_desc_tag = prod_desc_parent_tag.find("div", {"class": "value"})
                if prod_desc_tag:
                    prod_desc = (
                        prod_desc_tag.text.strip()
                        .replace("'", "")
                        .replace("\n", " ")
                        .replace("\r", "")
                    )
                    product["prod_desc"] = prod_desc
                else:
                    log.info("No se pudo obtener prod_desc 2")
            else:
                log.info("No se pudo obtener prod_desc parent")

        # UPC
        upc = ""
        upc_tag = prod_soup.find("td", {"data-th": "UPC"})
        if upc_tag:
            upc = upc_tag.text.strip().replace("'", "")
            product["upc"] = upc
        else:
            log.info("No se pudo obtener upc")

        # PICTURES
        pictures = []
        time.sleep(1)
        pictures_parent_tag = prod_soup.find(
            "div", {"class": "fotorama__stage"}
        )
        if pictures_parent_tag:
            picture_tags = pictures_parent_tag.find_all(
                "img", {"class": "fotorama__img"}
            )
            if picture_tags:
                for pic_tag in picture_tags:
                    pictures.append(pic_tag["src"])

                pics_json = json.dumps(pictures)
                product["pictures"] = pics_json
            else:
                log.info("No se pudieron obtener imagenes")
        else:
            log.info("No se pudo obtener el parent de imagenes")


        # PRICE
        price = 0.0
        price_tag = prod_soup.find("span", {"class": "price"})
        if price_tag:
            price = float(
                price_tag.text.strip()
                .replace("'", "")
                .replace("$", "")
                .replace(",", "")
            )
            product["price"] = price
        else:
            log.info("No se pudo obtener price")

        product["scrape_datetime"] = datetime.now()

        if product["sku"] not in existing_products:
            insert_product(connection, product)
            log.info("Producto insertado correctamente...")
        else:
            log.info("Producto previamente scrapeado. Ignorando...")
        log.info(product)
        log.info("")
        log.info("")
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        log.info("")
        log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS START!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "GROWTH_ANALYSTS",
        "role": "GROWTH_ROLE",
        "database": "fivetran",
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    get_existing_products(connection)
    try:
        with open("categories.csv", "r") as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                cat_url = row["category_url"]
                start_scraping(cat_url, connection)

        driver.close()
    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
