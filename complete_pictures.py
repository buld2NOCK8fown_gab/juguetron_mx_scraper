"""
Author: Alain Moré Maceda
Scraper de productos Juguetron MX
"""
import csv
import json
import logging
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_complete_pics.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications": 2}
chrome_opt.add_experimental_option("prefs", prefs)
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 10)
actions = ActionChains(driver)

existing_products = []


def get_existing_products(connection):
    params = dict(COUNTRY='MX')

    select = """
       SELECT URL FROM {{COUNTRY}}_WRITABLE.JUGUETRON_PRODUCTS
       WHERE PICTURES = '';
    """
    #select = """
    #    SELECT URL FROM MX_WRITABLE.JUGUETRON_PRODUCTS
    #    WHERE SKU = '154086016-5'
    #"""
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(select)
    formatted_template = template.render(**params)
    log.info(formatted_template)
    try:
        df_existing_products = pd.read_sql_query(
            con=connection, sql=formatted_template)
        log.info(df_existing_products)
        for element in df_existing_products.values.tolist():
            existing_products.append(element[0])
    except Exception as ex:
        log.error(ex)


def get_dom(url):
    """
    gets the DOM of the requested url using selenium
    """
    driver.get(url)


def update_product(connection, url, pics_json):
    """
    updates current processed product into Snowflake
    """
    update = """
        UPDATE MX_WRITABLE.JUGUETRON_PRODUCTS 
        SET PICTURES = '""" + pics_json + """'
        WHERE URL = '""" + url + """'
    """
    product = {}
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(update)
    formatted_template = template.render(**product)
    log.info(formatted_template)
    try:
        df_update = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("update OK")
        log.info(df_update)
    except Exception as ex:
        log.info("update ERROR")
        log.error(ex)


def start_scraping(url, connection):
    """
    Scraping main function, orchestrates full process
    """
    get_dom(url)
    time.sleep(1)
    prod_soup = BeautifulSoup(driver.page_source, "html.parser")

    # PICTURES
    pictures = []
    time.sleep(1)
    pictures_parent_tag = prod_soup.find(
        "div", {"class": "fotorama__stage"}
    )
    if pictures_parent_tag:
        picture_tags = pictures_parent_tag.find_all(
            "img", {"class": "fotorama__img"}
        )
        if picture_tags:
            for pic_tag in picture_tags:
                pictures.append(pic_tag["src"])

            pics_json = json.dumps(pictures)
        else:
            log.info("No se pudieron obtener imagenes")
    else:
        log.info("No se pudo obtener el parent de imagenes")

    update_product(connection, url, pics_json)
    log.info("Producto actualizado correctamente...")
    log.info(url)
    log.info(pics_json)
    log.info("")
    log.info("")
    log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    log.info("")
    log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS START!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "GROWTH_ANALYSTS",
        "role": "GROWTH_ROLE",
        "database": "fivetran",
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    get_existing_products(connection)
    try:
        for product_url in existing_products:
            start_scraping(product_url, connection)

        driver.close()
    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
